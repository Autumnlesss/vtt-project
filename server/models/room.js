const mongoose = require('mongoose');
const User = require('../models/user');

const roomSchema = new mongoose.Schema({
  roomId: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  backgroundImageUrl: {
    type: String,
    required: false
  },
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }]
});

const Room = mongoose.model('Room', roomSchema);

module.exports = Room;
