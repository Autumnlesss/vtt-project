/* Connect to MongoDB */
const mongoose = require('mongoose');
require('./db/mongoose');

/* Express */
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');

/* Socket IO */
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: true,
    credentials: true,
  },
  allowEIO3: true,
});

/* Routes */
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const messagesRouter = require('./routes/message');
const roomRoutes = require('./routes/room');

app.use(cors({
  origin: ['http://localhost:3000', 'http://localhost:8080'],
  credentials: true
}));
app.use(cookieParser());
app.use(express.json());

/* Routes Defenitions */
app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);
app.use('/api/message', messagesRouter);
app.use('/api/room', roomRoutes);

const { createRoom, getRoom } = require('./middleware/room');
const { saveMessage } = require('./middleware/messages');
const { sendChatHistory } = require('./controllers/sendChatHistory');

const crypto = require('crypto');

const Room = require('./models/room');

const tokenSizes = {
  'token': 50,
  'token l': 100,
  'token h': 150,
  'token g': 200
};

app.use('/images', express.static('images'));

io.on('connection', async function (socket) {
  console.log(`${socket.id} connected`);

  socket.on("joinRoom", async (roomId, userId) => {
    try {
      const room = await getRoom(roomId);
      if (room) {
        socket.join(roomId);

        await addUserIdToRoom(roomId, userId);
        io.to(roomId).emit('userAdded', userId);

        sendChatHistory(socket, roomId);

      } else {
        console.log("Room not found:", roomId);
      }
    } catch (error) {
      console.error("Error joining room:", error);
    }
  });

  socket.on('send', async (text, roomId, username) => {
    if (Object.keys(tokenSizes).includes(text)) {
      const tokenId = generateUniqueId();
      const size = tokenSizes[text];
      const randomColor = `0x${Math.floor(Math.random() * 16777215).toString(16)}`;
      io.to(roomId).emit('create', size, size, tokenId, randomColor);
    } else {
      let newText = { text: text, roomId: roomId, username: username };

      io.to(roomId).emit('receive', newText);

      saveMessage(newText);
    }
  });

  socket.on('roll_dice', async (roomId, sides, username) => {
    const roll = Math.floor(Math.random() * sides) + 1;
    let rollResult = `D${sides} = ${roll}`;
    let newText = { text: rollResult, roomId: roomId, username: username };

    io.to(roomId).emit('receive', newText);

    saveMessage(newText);
  });

  socket.on('setBackgroundImage', (roomId, imageUrl) => {
    socket.to(roomId).emit('setBackgroundImage', imageUrl);
  });

  socket.on('updatePosition', (roomId, dragX, dragY, tokenId) => {
    socket.to(roomId).emit('drag', dragX, dragY, tokenId);
  });

  socket.on('getUsersInRoom', async (roomId, callback) => {
    try {
      const room = await getRoom(roomId);
      if (room) {
        const users = await getUsersInRoom(roomId);
        callback(users);
        io.to(roomId).emit('usersReceived', users);
      } else {
        callback([]);
      }
    } catch (error) {
      console.error("Error getting users in room:", error);
      callback([]);
    }
  });

  socket.on('leaveRoom', async function (roomId, userId) {
    try {
      await removeUserIdFromRoom(roomId, userId);
      io.to(roomId).emit('userRemoved', userId);
    } catch (error) {
      console.error("Error deleting a user from a room:", error);
    }
  });

  socket.on('disconnect', function () {
    console.log('A user disconnected: ' + socket.id);
  });
});

function generateUniqueId() {
  return crypto.randomBytes(16).toString('hex');
}

async function addUserIdToRoom(roomId, userId) {
  if (userId === null) {
    console.error("Error adding a user to the room: userId cannot be null");
    return;
  }
  try {
    await Room.updateOne({ roomId }, { $addToSet: { users: userId } });
  } catch (error) {
    console.error("Error adding user to room:", error);
  }
}

async function removeUserIdFromRoom(roomId, userId) {
  try {
    await Room.updateOne({ roomId }, { $pull: { users: userId } });
  } catch (error) {
    console.error("Error deleting user from room:", error);
  }
}

async function getUsersInRoom(roomId) {
  try {
    const room = await Room.findOne({ roomId });
    if (room) {
      if (room.users === null) {
        console.log("Users not found");
        return [];
      } else {
        return room.users;
      }
    } else {
      console.log(`Room not found`);
      return [];
    }
  } catch (error) {
    console.error("Error getting users from room:", error);
    return [];
  }
}

/* Starting node server */
const PORT = process.env.PORT || 3000
server.listen(PORT, 'adv-server-autumnlesss.amvera.io',() => {
  console.log(`Server started on https://adv-server-autumnlesss.amvera.io/`);
});

