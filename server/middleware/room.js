const Room = require('../models/room');

async function createRoom(room) {
    try {
      if (!room.name || !room.description) {
        throw new Error("Room data is invalid");
      }
      const newRoom = new Room(room);
      await newRoom.save();
      return newRoom;
    } catch (error) {
      console.error("Error creating room:", error);
      throw error;
    }
  }
  
  async function getRoom(roomId) {
    try {
      const room = await Room.findOne({ roomId });
      return room;
    } catch (error) {
      console.error('Error getting room:', error);
      throw error;
    }
  }

  module.exports = {
    createRoom,
    getRoom
  };