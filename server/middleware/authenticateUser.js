const jwt = require('jsonwebtoken')
const User = require('../models/user')

module.exports = async (req, res, next) => {
  try {
    const token = req.cookies['jwt'];

    const claims = jwt.verify(token, 'secret');

    if (!claims) {
      return res.status(401).send({
        message: 'unauthenticated'
      });
    }

    const user = await User.findOne({ _id: claims._id });

    if (!user) {
      return res.status(404).send({
        message: 'user not found'
      });
    }

    req.user = user;
    next();
  } catch (e) {
    return res.status(400).send({
      message: 'unauthenticated'
    });
  }
}