const Message = require('../models/message');

async function saveMessage(newText) {
    try {
      const message = new Message(newText);
      await message.save();
    } catch (error) {
      console.error('Error saving message:', error);
    }
  }

  module.exports = {
    saveMessage
  };