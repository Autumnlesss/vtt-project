const mongoose = require('mongoose');
require('dotenv').config();

const username = process.env.MONGO_USERNAME;
const password = process.env.MONGO_PASSWORD;
const db_name = process.env.MONGO_DB_NAME;

const connect = () => {
    mongoose.connect(`mongodb://${username}:${password}@amvera-autumnlesss-run-adv-db/${db_name}`)
    .then(() => console.log('Connected to database'))
    .catch((err) => console.error(err));
}

connect();

module.exports = {mongoose, connect};