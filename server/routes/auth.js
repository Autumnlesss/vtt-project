const router = require('express').Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/user')

/* register route */
router.post('/register', async (req, res) => {
    
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    })

    const result = await user.save()

    const {password, ...data} = await result.toJSON()

    res.send(data)
})

/* login route*/
router.post('/login', async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    
    if (!user) {
        return res.status(404).send({
            message: 'user not found'
        });
    }

    if (!await bcrypt.compare(req.body.password, user.password)) {
        return res.status(400).send({
            message: 'invalid credentials'
        });
    }

    const token = jwt.sign({
        _id: user._id, 
        email: user.email 
    }, "secret");

    res.cookie('jwt', token, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000
    });

    res.send({
        message: 'success'
    });
});

/* logout route*/
router.post('/logout', (req, res) => {
    res.cookie('jwt', '', {maxAge:0})

    res.send({
        message: 'success'
    })
})

module.exports = router;