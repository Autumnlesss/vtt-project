const router = require('express').Router()
const authenticateUser = require('../middleware/authenticateUser')
const User = require('../models/user')

router.get('/user', authenticateUser, async (req, res) => {
    try {
        const { password, ...data } = req.user.toJSON();

        res.send(data);
    } catch (e) {
        return res.status(400).send({
            message: 'unauthenticated'
        });
    }
});

router.get('/user-info', authenticateUser, async (req, res) => {
    try {
        const { password, ...data } = req.user.toJSON();
        const userInfo = {
            name: data.name,
            description: data.description,
            avatarFlag: data.avatarFlag
        };

        res.send(userInfo);
    } catch (e) {
        return res.status(400).send({
            message: 'unauthenticated'
        });
    }
});

router.post('/user-info', authenticateUser, async (req, res) => {
    try {
        req.user.name = req.body.name || req.user.name;
        req.user.description = req.body.description || req.user.description;
        await req.user.save();

        res.send({
            message: 'user info updated',
            name: req.user.name,
            description: req.user.description
        });
    } catch (e) {
        return res.status(400).send({
            message: 'unauthenticated'
        });
    }
});

router.post('/avatar', authenticateUser, async (req, res) => {
    try {
        const user = await User.findById(req.user.id);
        if (!user) {
            return res.status(404).send({
                message: 'User not found'
            });
        }
        const base64 = req.body.avatar.replace(/^data:image\/jpeg;base64,/, '');
        const avatarBuffer = Buffer.from(base64, 'base64');
        user.avatar = avatarBuffer;
        user.avatarFlag = req.body.avatarFlag;
        await user.save();
        res.send({
            message: 'avatar updated'
        });
    } catch (e) {
        console.error(e);
        res.status(500).send({
            message: 'Server error'
        });
    }
});

router.get('/avatar', authenticateUser, async (req, res) => {
    try {
        const userId = req.user.id;
        const user = await User.findById(userId);
        if (!user || !user.avatar || !user.avatarFlag) {
            return res.status(404).send({
                message: 'Avatar not found'
            });
        }

        res.set('Content-Type', 'image/jpeg');
        res.send(user.avatar);
    } catch (error) {
        console.error(error);
        res.status(500).send({
            message: 'Server error'
        });
    }
});

router.get('/player-avatar', async (req, res) => {
    try {
        const userId = req.query.userId;
        const user = await User.findById(userId);
        if (!user || !user.avatar || !user.avatarFlag) {
            return res.status(404).send({
                message: 'Avatar not found'
            });
        }

        res.set('Content-Type', 'image/jpeg');
        res.send(user.avatar);
    } catch (error) {
        console.error(error);
        res.status(500).send({
            message: 'Server error'
        });
    }
});

module.exports = router;