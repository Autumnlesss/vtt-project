const express = require('express');
const Message = require('../models/message');

const router = express.Router();

router.get('/messages/:roomId', async (req, res) => {
    const { roomId } = req.params;
    try {
        const messages = await Message.find({ roomId }).sort({ timestamp: 1 });
        res.json(messages);
    } catch (error) {
        console.error('Error receiving messages:', error);
        res.status(500).json({ error: 'Failed to receive messages' });
    }
});

module.exports = router;