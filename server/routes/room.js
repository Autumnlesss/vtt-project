const express = require('express');
const router = express.Router();
const Room = require('../models/room');
const multer = require('multer');
const path = require('path');
const uuid = require('uuid');
const fs = require('fs')

const upload = multer({
  dest: './images/',
  filename: (req, file, cb) => {
    const ext = path.extname(file.originalname);
    const filename = `${uuid.v4()}${ext}`;
    cb(null, filename);
  }
});


router.post('/create', async (req, res) => {
  try {
    const { name, description, roomId } = req.body;

    const room = new Room({
      name,
      description,
      roomId
    });

    await room.save();

    res.status(201).json({ message: 'Room created' });
  } catch (error) {

    console.error('Error creating a room:', error);
    res.status(500).json({ message: 'Failed to create a room' });
  }
});

router.get('/checkRoom/:roomId', async (req, res) => {
  const roomId = req.params.roomId;
  try {
    const room = await Room.findOne({ roomId });
    if (room) {

      res.status(200).json({ exists: true });
    } else {

      res.status(404).json({ exists: false });
    }
  } catch (error) {
    console.error('Error checking the room:', error);
    res.status(500).json({ error: 'Failed to find a room' });
  }
});

router.post('/uploadImage', upload.single('image'), async (req, res) => {
  const file = req.file;
  const roomId = req.body.roomId;
  const imageUrl = `images/${file.filename}`;

  try {
    const room = await Room.findOne({ roomId });
    if (!room) {
      return res.status(404).json({ error: 'An entry with the specified roomId was not found' });
    }

    if (room.backgroundImageUrl) {
      const oldImagePath = `images/${room.backgroundImageUrl.split('/').pop()}`;
      fs.unlinkSync(oldImagePath);
    }

    room.backgroundImageUrl = imageUrl;
    await room.save();
    res.json({ imageUrl });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error updating a record in the database' });
  }
});

router.get('/getImage/:roomId', async (req, res) => {
  const roomId = req.params.roomId;
  try {
    const room = await Room.findOne({ roomId });
    if (!room) {
      return res.status(404).json({ error: 'An entry with the specified roomId was not found' });
    }

    const imageUrl = room.backgroundImageUrl;
    res.json({imageUrl});

  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error searching for a record in the database' });
  }
});

module.exports = router;
