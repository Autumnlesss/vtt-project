const Message = require('../models/message');

async function sendChatHistory(socket, roomId) {
    try {
      const messages = await Message.find({ roomId }).sort({ timestamp: 1 });
      socket.emit('history', { messages, roomId });
    } catch (error) {
      console.error('Error sending history:', error);
    }
  }

  module.exports = {
    sendChatHistory
  };