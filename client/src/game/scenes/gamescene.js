import Phaser from 'phaser';

export default class GameScene extends Phaser.Scene {
    constructor(roomId, socket) {
        super({
            key: 'GameScene'
        });
        this.roomId = roomId;
        this.tokens = [];
        this.socket = socket;
    }

    setBackgroundImage(backgroundImage) {
        const { textures, add } = this;
      
        textures.remove('background');
        this.backgroundImageObject?.destroy();
      
        fetch(backgroundImage)
          .then(response => response.blob())
          .then(blob => {
            const img = new Image();
            img.src = URL.createObjectURL(blob);
            img.onload = () => {
              textures.addImage('background', img);
              this.backgroundImageObject = add.image(0, 0, 'background').setOrigin(0, 0);
            };
          })
          .catch(error => {
            console.error('Error loading image:', error);
          });
      }

    create() {
        const minZoom = 0.25;
        const maxZoom = 1.0;
        let isDragging = false;
        let dragStartX = 0;
        let dragStartY = 0;
        this.cameras.main.zoom = maxZoom;
        
        
        this.socket.on('create', (width, height, tokenId, randomColor) => {
            try {    
                const token = this.add.rectangle(100, 100, width, height, randomColor).setInteractive();
                token.setStrokeStyle(1, 0x000000, 1);
                token.tokenId = tokenId;
                this.tokens.push(token);
                this.input.setDraggable(token);

                
            } catch (error) {
                console.error("Error adding token:", error);
            }


            this.socket.on('drag', (dragX, dragY, tokenId) => {
                const foundToken = this.tokens.find(t => t.tokenId === tokenId);
                if (foundToken) {
                    foundToken.x = dragX;
                    foundToken.y = dragY;
                }
            });
        });

        this.input.on('drag', (pointer, token, dragX, dragY) => {
            const foundToken = this.tokens.find(t => t.tokenId === token.tokenId);
            if (foundToken) {
                foundToken.x = dragX;
                foundToken.y = dragY;
            }

            this.socket.emit('updatePosition', this.roomId, dragX, dragY, token.tokenId);
        });

        this.input.on('wheel', (pointer) => {
            const camera = this.cameras.main;
            const cursorX = pointer.x;
            const cursorY = pointer.y;
            let newZoom = camera.zoom - (pointer.deltaY * 0.001);
            newZoom = Math.min(Math.max(newZoom, minZoom), maxZoom);


            const offsetX = (cursorX - camera.scrollX) * (camera.zoom - newZoom) / camera.zoom;
            const offsetY = (cursorY - camera.scrollY) * (camera.zoom - newZoom) / camera.zoom;


            camera.setScroll(camera.scrollX - offsetX, camera.scrollY - offsetY);

            camera.zoom = newZoom;
        });

        this.input.on('pointerdown', (pointer) => {
            if (pointer.middleButtonDown()) {
                isDragging = true;
                dragStartX = pointer.x;
                dragStartY = pointer.y;
            }
        });

        this.input.on('pointermove', (pointer) => {
            if (isDragging) {
                const camera = this.cameras.main;
                const deltaX = (pointer.x - dragStartX) * 2;
                const deltaY = (pointer.y - dragStartY) * 2;
                camera.setScroll(camera.scrollX - deltaX, camera.scrollY - deltaY);
                dragStartX = pointer.x;
                dragStartY = pointer.y;
            }
        });

        this.input.on('pointerup', () => {
            isDragging = false;
        });

        this.socket.on('setBackgroundImage', (imageUrl) => {
            this.setBackgroundImage(imageUrl);
          });
    }
}