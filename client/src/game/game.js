import Phaser from "phaser";
import GameScene from "./scenes/gamescene";

function launch(containerId, roomId, socket) {
    return new Phaser.Game({
        type: Phaser.AUTO,
        parent: containerId,
        scene: [
            new GameScene(roomId, socket)
        ],
        scale: {
            mode: Phaser.Scale.FIT,
            width: '100%',
            height: '100%'
        },
        backgroundColor: '#FFFFFF',
    });
}

export default launch;
export { launch }