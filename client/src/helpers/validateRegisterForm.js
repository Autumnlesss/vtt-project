export function validateRegisterForm(data) {
    const { name, email, password } = data;

    if (!name) {
        alert('Введите имя пользователя');
        return false;
    } else if (!/^[a-zA-Zа-яА-Я0-9]+$/.test(name) || name.length < 3 || name.length > 20) {
        alert('Имя пользователя должно состоять из русских, латинских букв и цифр, и иметь длину от 3 до 20 символов');
        return false;
    }

    if (!email) {
        alert('Введите адрес электронной почты');
        return false;
    } else if (email.length > 40) {
        alert('Адрес электронной почты должен быть не больше 40 символов');
        return false;
    }

    if (!password) {
        alert('Введите пароль');
        return false;
    } else if (password.length < 8 || password.length > 50) {
        alert('Пароль должен быть от 8 до 50 символов');
        return false;
    }

    return true;
}