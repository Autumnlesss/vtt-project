import { useStore } from 'vuex';

async function authenticateUser() {
    const store = useStore();
    try {
      const response = await fetch('http://localhost:3000/api/user/user', {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include'
      });

      
  
      if (response.ok) {
        const userData = await response.json();
        await store.dispatch('setAuth', true);
        await store.dispatch('setUserId', userData._id); 
        return true;
      } else {
        await store.dispatch('setAuth', false);
        return false;
      }
    } catch (error) {
      await store.dispatch('setAuth', false);
      return false;
    }
  }
  
  export { authenticateUser };