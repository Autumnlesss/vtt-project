import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios';
import { defineCustomElements as defineIonPhaser } from '@ion-phaser/core/loader';
import Phaser from 'phaser';

axios.defaults.baseURL = 'http://localhost:3000/api';

defineIonPhaser(window);

createApp(App)
 .provide('phaser', Phaser)
 .use(store)
 .use(router)
 .mount('#app');