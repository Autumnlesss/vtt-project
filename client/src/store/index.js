import { createStore } from 'vuex'

export default createStore({
  state: {
    authenticated: false,
    socket: null,
    roomId: null,
    userId: null
  },
  getters: {
  },
  mutations: {
    SET_AUTH: (state, auth) => {
      state.authenticated = auth;
    },
    setSocket(state, socket) {
      state.socket = socket;
    },
    setRoomId(state, roomId) {
      state.roomId = roomId;
    },
    setUserId(state, userId) {
      state.userId = userId;
    },
  }, 
  actions: {
    setAuth: ({ commit }, auth) => commit('SET_AUTH', auth),
    setUserId: ({ commit }, userId) => commit('setUserId', userId)
  },
  modules: {
  }
})
